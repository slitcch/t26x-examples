#include <time.h>

#include <iostream>
#include <chrono>
#include <thread>
#include <future>

#include <opencv2/opencv.hpp>
#include <librealsense2/rs.hpp>

uint64_t frame_id = 0;

// Cargo-culted from Monado
#define U_1_000_000_000 (1000000000)

static inline uint64_t
os_timespec_to_ns(const struct timespec *spec)
{
	uint64_t ns = 0;
	ns += (uint64_t)spec->tv_sec * U_1_000_000_000;
	ns += (uint64_t)spec->tv_nsec;
	return ns;
}

double last_gyro_ts;
double last_accel_ts;

double last_camera_ts;

int main()
{

	auto our_callback = [&](const rs2::frame &frame) {
		double now_ts = frame.get_timestamp();
		if (rs2::motion_frame motion_frame = frame.as<rs2::motion_frame>())
		{
			if (motion_frame.get_profile().stream_type() == RS2_STREAM_GYRO)
			{
				rs2_vector gyro_vec = motion_frame.get_motion_data();
				printf("Got gyro frame  idx %7llu ts %f, % 7.02f hz, vec % 7.03f, % 7.03f, % 7.03f\n", motion_frame.get_frame_number(), now_ts, 1000 / (now_ts - last_gyro_ts), gyro_vec.x, gyro_vec.y, gyro_vec.z);
				last_gyro_ts = now_ts;
			}
			else if (motion_frame.get_profile().stream_type() == RS2_STREAM_ACCEL)
			{
				rs2_vector accel_vec = motion_frame.get_motion_data();
				printf("Got accel frame idx %7llu ts %f, % 7.02f hz, vec % 7.03f, % 7.03f, % 7.03f\n", motion_frame.get_frame_number(), now_ts, 1000 / (now_ts - last_accel_ts), accel_vec.x, accel_vec.y, accel_vec.z);
				last_accel_ts = now_ts;
			}
		}

		else if (auto frames = frame.as<rs2::frameset>())
		{
			printf("Got video frame idx %7llu ts %f, % 7.02f hz\n", frames.get_fisheye_frame(1).get_frame_number(), now_ts, 1000 / (now_ts - last_camera_ts));

			cv::Mat fisheye_left = cv::Mat(cv::Size(848, 800), CV_8UC1, (void *)frames.get_fisheye_frame(1).get_data(), cv::Mat::AUTO_STEP);
			cv::Mat fisheye_right = cv::Mat(cv::Size(848, 800), CV_8UC1, (void *)frames.get_fisheye_frame(2).get_data(), cv::Mat::AUTO_STEP);

			cv::imshow("video", fisheye_left);
			cv::imshow("video1", fisheye_right);
			cv::waitKey(1);
			last_camera_ts = now_ts;
		}
	};

	rs2::pipeline pipe;
	rs2::config cfg;

	cfg.enable_stream(rs2_stream::RS2_STREAM_FISHEYE, 1);
	cfg.enable_stream(rs2_stream::RS2_STREAM_FISHEYE, 2);
	cfg.enable_stream(rs2_stream::RS2_STREAM_ACCEL, RS2_FORMAT_MOTION_XYZ32F); // hopefully defaults to 62.5hz
	cfg.enable_stream(rs2_stream::RS2_STREAM_GYRO, RS2_FORMAT_MOTION_XYZ32F);	 // hopefully defaults to 200hz

	rs2::pipeline_profile profiles = cfg.resolve(pipe);

	rs2::sensor cameras = profiles.get_device().query_sensors()[0];

	cameras.set_option(RS2_OPTION_ENABLE_AUTO_EXPOSURE, 0.0f);
	cameras.set_option(RS2_OPTION_EXPOSURE, 500.0f);
	cameras.set_option(RS2_OPTION_GAIN, 2.0f);

	profiles = pipe.start(cfg, our_callback);

	// rs2::pipeline_profile profiles = pipe.start(cfg, [&](rs2::frame frame) { my_member_function(frame);  });

	rs2::video_stream_profile camera_left_prof = static_cast<rs2::video_stream_profile>(profiles.get_stream(RS2_STREAM_FISHEYE, 1));

	rs2::video_stream_profile camera_right_prof = static_cast<rs2::video_stream_profile>(profiles.get_stream(RS2_STREAM_FISHEYE, 2));

	rs2::motion_stream_profile accel_prof = static_cast<rs2::motion_stream_profile>(profiles.get_stream(RS2_STREAM_ACCEL));

	rs2::motion_stream_profile gyro_prof = static_cast<rs2::motion_stream_profile>(profiles.get_stream(RS2_STREAM_GYRO));

	rs2_intrinsics left_int = camera_left_prof.get_intrinsics();
	rs2_extrinsics left_ext = camera_left_prof.get_extrinsics_to(gyro_prof);
	rs2_extrinsics left_ext_accel = camera_left_prof.get_extrinsics_to(accel_prof);

	printf("========= LEFT =========\n");
	printf("distortion coeffs: \n%f %f %f %f %f\n\n", left_int.coeffs[0], left_int.coeffs[1], left_int.coeffs[2], left_int.coeffs[3], left_int.coeffs[4]);
	printf("camera matrix: \n[% 7.5f % 7.5f % 7.5f]\n[% 7.5f % 7.5f % 7.5f]\n[% 7.5f % 7.5f % 7.5f]\n\n", left_int.fx, 0.f, left_int.ppx,
				 0.f, left_int.fy, left_int.ppy,
				 0.f, 0.f, 1.f);
	printf("translation to IMU: \n[% 7.5f % 7.5f % 7.5f]\n\n", left_ext.translation[0], left_ext.translation[2], left_ext.translation[2]);
	printf("rotation to IMU: \n[% 7.5f % 7.5f % 7.5f]\n[% 7.5f % 7.5f % 7.5f]\n[% 7.5f % 7.5f % 7.5f]\n",
				 left_ext.rotation[0], left_ext.rotation[1], left_ext.rotation[2],
				 left_ext.rotation[3], left_ext.rotation[4], left_ext.rotation[5],
				 left_ext.rotation[6], left_ext.rotation[7], left_ext.rotation[8]);
	// std::cout << left_ext.rotation << left_ext.translation << "\n";

	rs2_intrinsics right_int = camera_right_prof.get_intrinsics();
	rs2_extrinsics right_ext = camera_right_prof.get_extrinsics_to(gyro_prof);

	printf("\n\n\n========= RIGHT =========\n");
	printf("distortion coeffs: \n%f %f %f %f %f\n\n", right_int.coeffs[0], right_int.coeffs[1], right_int.coeffs[2], right_int.coeffs[3], right_int.coeffs[4]);
	printf("camera matrix: \n[% 7.5f % 7.5f % 7.5f]\n[% 7.5f % 7.5f % 7.5f]\n[% 7.5f % 7.5f % 7.5f]\n\n", right_int.fx, 0.f, right_int.ppx,
				 0.f, right_int.fy, right_int.ppy,
				 0.f, 0.f, 1.f);
	printf("translation to IMU: \n[% 7.5f % 7.5f % 7.5f]\n\n", right_ext.translation[0], right_ext.translation[2], right_ext.translation[2]);
	printf("rotation to IMU: \n[% 7.5f % 7.5f % 7.5f]\n[% 7.5f % 7.5f % 7.5f]\n[% 7.5f % 7.5f % 7.5f]\n",
				 right_ext.rotation[0], right_ext.rotation[1], right_ext.rotation[2],
				 right_ext.rotation[3], right_ext.rotation[4], right_ext.rotation[5],
				 right_ext.rotation[6], right_ext.rotation[7], right_ext.rotation[8]);

	float time = 0.0;
	while (true)
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(25));
		time += 0.05;
		cameras.set_option(RS2_OPTION_EXPOSURE, (cos(time)*50000)); // Shows how to set the exposure live
	}
}
