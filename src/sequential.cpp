#include <time.h>

#include <iostream>
#include <chrono>
#include <thread>
#include <future>

#include <opencv2/opencv.hpp>
#include <librealsense2/rs.hpp>

uint64_t frame_id = 0;

// Cargo-culted from Monado
#define U_1_000_000_000 (1000000000)

static inline uint64_t
os_timespec_to_ns(const struct timespec *spec)
{
	uint64_t ns = 0;
	ns += (uint64_t)spec->tv_sec * U_1_000_000_000;
	ns += (uint64_t)spec->tv_nsec;
	return ns;
}

int main()
{
	// Shoutout to Damien Rompapas (https://github.com/HyperLethalVector) for the Realsense code I'm copying:
	rs2::pipeline pipe;
  rs2::config cfg;
  rs2::pipeline_profile myProf; 
  rs2::context ctx; 
	
  // cfg.enable_stream(rs2_stream::RS2_STREAM_FISHEYE, 1, 30);
  // cfg.enable_stream(rs2_stream::RS2_STREAM_FISHEYE, 2, 30);
  // cfg.enable_stream(rs2_stream::RS2_STREAM_ACCEL, RS2_FORMAT_MOTION_XYZ32F, 200);
  // cfg.enable_stream(rs2_stream::RS2_STREAM_GYRO, RS2_FORMAT_MOTION_XYZ32F, 200);
	// cfg.enable_stream(RS2_STREAM_POSE, RS2_FORMAT_6DOF, 200);
	// XXX: for whatever reason, can't get the above to work so everything appears to be locked to 30hz :(
	// May just be a limitation of not using the callback mode

  cfg.enable_stream(rs2_stream::RS2_STREAM_FISHEYE, 1);
  cfg.enable_stream(rs2_stream::RS2_STREAM_FISHEYE, 2);
  cfg.enable_stream(rs2_stream::RS2_STREAM_ACCEL, RS2_FORMAT_MOTION_XYZ32F);
  cfg.enable_stream(rs2_stream::RS2_STREAM_GYRO, RS2_FORMAT_MOTION_XYZ32F);
	cfg.enable_stream(RS2_STREAM_POSE, RS2_FORMAT_6DOF);
	// cfg.enable_all_streams();
	int64_t already = 0;

	auto profile = pipe.start(cfg);



	std::this_thread::sleep_for(std::chrono::milliseconds(300));

  cv::Ptr<cv::Formatter> fmt = cv::Formatter::get(cv::Formatter::FMT_DEFAULT);
  fmt->set64fPrecision(3);
  fmt->set32fPrecision(3);

	int64_t last_time;
	int64_t the_time;

	while (true) {
		rs2::frameset frames = pipe.wait_for_frames();

			// XXX: Don't use computer's timestamp
		struct timespec ts;
		int ret = clock_gettime(CLOCK_MONOTONIC, &ts);
		if (ret != 0) {
			puts("Invalid time. This really shouldn't happen.");
			return 1;
		}
		int64_t the_time = os_timespec_to_ns(&ts);

		std::cout << "size is " << frames.size() << "\n";

		rs2::video_frame fisheye_left = frames.get_fisheye_frame(1);
		rs2::video_frame fisheye_right = frames.get_fisheye_frame(2);

		// rs2::frame has a bool operator that returns false if the frame is empty
		// ie. if this is giving us an IMU value and not fisheye frames.
		if (fisheye_left && fisheye_right){ // && (fisheye_left.get_frame_number != last_frame_number)
			std::cout << ("Got fisheye frames idx ") << fisheye_left.get_frame_number() << "\n";

			cv::Mat fisheye_left  = cv::Mat(cv::Size(848,800), CV_8UC1, (void*)frames.get_fisheye_frame(1).get_data(), cv::Mat::AUTO_STEP);
			cv::Mat fisheye_right = cv::Mat(cv::Size(848,800), CV_8UC1, (void*)frames.get_fisheye_frame(2).get_data(), cv::Mat::AUTO_STEP);

			cv::imshow("video",fisheye_left);
			cv::imshow("video1",fisheye_right);
			cv::waitKey(1);
		}

		rs2::motion_frame gyro_frame = frames.first_or_default(RS2_STREAM_GYRO);
		rs2::motion_frame accel_frame = frames.first_or_default(RS2_STREAM_ACCEL);

		if (gyro_frame)
		{
			if (!(gyro_frame.get_profile().stream_type() == RS2_STREAM_GYRO)) puts("Gyro stream type wrong."); // Shouldn't ever happen in my experience -Moses Turner
			rs2_vector gyro_vec = gyro_frame.get_motion_data();
			printf("Got gyro frame  % 7.03f, % 7.03f, % 7.03f\n", gyro_vec.x, gyro_vec.y, gyro_vec.z);
		}

		if (accel_frame)
		{
			if (!(accel_frame.get_profile().stream_type() == RS2_STREAM_ACCEL)) puts("Gyro stream type wrong."); // ditto.
			rs2_vector accel_vec = accel_frame.get_motion_data();
			printf("Got accel frame % 7.03f, % 7.03f, % 7.03f\n", accel_vec.x, accel_vec.y, accel_vec.z);
		}

	}
}
